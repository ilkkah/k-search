/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS103: Rewrite code to no longer use __guard__, or convert again using --optional-chaining
 * DS206: Consider reworking classes to avoid initClass
 * DS207: Consider shorter variations of null checks
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/main/docs/suggestions.md
 */

let Search;
module.exports = (Search = (function() {
	Search = class Search {
		constructor() {
			this.keydown = this.keydown.bind(this);
			this.changed = this.changed.bind(this);
		}

		static initClass() {
			this.prototype.view = __dirname;
			this.prototype.name = 'k-search';
		}

		init() {
			const {
                model
            } = this;
			model.set('show', '');
			model.set('position', -1);
			model.on('change', 'search', this.changed);
			const cache = model.root.get('_cache.search');

			if (cache) {
				return model.set('people', cache);
			} else {
				const peopleq = model.root.at('misc.people');
				return peopleq.fetch(function(err) {
					const people = peopleq.get('list');
					model.set('people', people);
					return model.root.set('_cache.search', people);
				});
			}
		}

		submit(e) {
			e.preventDefault();
			const {
                model
            } = this;
			const {
                app
            } = this;
			const n = model.get('search').toLowerCase();

			const filterName = item => !n || (item.toLowerCase().indexOf(n) === 0);
			const people = model.get('people').filter(filterName);

			if (__guard__(app != null ? app.history : undefined, x => x.push)) {
				if (people.length === 1) {
					return app.history.push(`/${people[0]}`);
				} else if (people.length > 1) {
					return app.history.push(`/${n}`);
				} else if (model.toast) {
					return model.toast('error', 'ei löytynyt mtn.');
				}
			}
		}

		blur(e) {
			this.model.set('show', '');
			return true;
		}

		focus(e) {
			const n = __guard__(this.model.get('search'), x => x.toLowerCase());
			return this.model.set('show', n ? 'show' : '');
		}

		sel(e, s) {
			e.preventDefault();
			this.model.set('search', s);
			return this.submit(e);
		}

		keydown(e) {
			const {
                model
            } = this;
			const key = e.keyCode || e.which;
			const pos = model.get('position');

			// enter
			if ((key === 13) && (pos > -1)) {
				e.preventDefault();
				model.set('search', model.get(`people.${pos}`));
				model.set('position', -1);
			// up
			} else if ((key === 38) && (pos > -1)) {
				e.preventDefault();
				model.increment('position', -1);
			// down
			} else if ((key === 40) && (pos < __guard__(model.get('people'), x => x.length))) {
				e.preventDefault();
				model.increment('position', 1);
			}

			// scroll into view
			if (model.get('position') >= 0) {
				const el = document.getElementById(`id-${model.get('position')}`);
				const elBottomAt = el.offsetTop + el.offsetHeight;
				const container = el.offsetParent;
				if (container.scrollTop > el.offsetTop) {
					return container.scrollTop = el.offsetTop;
				} else if (elBottomAt > (container.offsetHeight + container.scrollTop)) {
					return container.scrollTop = elBottomAt - container.offsetHeight;
				}
			}
		}


		changed(e) {
			const {
                model
            } = this;
			const n = __guard__(model.get('search'), x => x.toLowerCase());
			model.set('show', n ? 'show' : '');

			const filterName = item => !n || (item.toLowerCase().indexOf(n) === 0);

			const people = model.root.get('_cache.search').filter(filterName);
			return model.set('people', people);
		}
	};
	Search.initClass();
	return Search;
})());


if (!Array.prototype.filter) {
  Array.prototype.filter = function(fun) { //, thisArg
    "use strict";
    if ((this === undefined) || (this === null)) { throw new TypeError(); }
    const t = Object(this);
    const len = t.length >>> 0;
    if (typeof fun !== "function") { throw new TypeError(); }
    const res = [];
    const thisArg = (arguments.length >= 2 ? arguments[1] : undefined);
    let i = 0;

    while (i < len) {
      if (i in t) {
        var val = t[i];
        
        // NOTE: Technically this should Object.defineProperty at
        //       the next index, as push can be affected by
        //       properties on Object.prototype and Array.prototype.
        //       But that method's new, and collisions should be
        //       rare, so use the more-compatible alternative.
        if (fun.call(thisArg, val, i, t)) { res.push(val); }
      }
      i++;
    }
    return res;
   };
 }

function __guard__(value, transform) {
  return (typeof value !== 'undefined' && value !== null) ? transform(value) : undefined;
}